/*
 * This file is part of the µOS++ distribution.
 *   (https://github.com/micro-os-plus)
 * Copyright (c) 2014 Liviu Ionescu.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

// ----------------------------------------------------------------------------

#include "cortexm/exception-handlers.h"

// ----------------------------------------------------------------------------
void __attribute__ ((section(".after_vectors")))
Default_Handler(void)
{
  while (1)
    {
      ;
    }
}

// Forward declaration of the specific IRQ handlers. These are aliased
// to the Default_Handler, which is a 'forever' loop. When the application
// defines a handler (with the same name), this will automatically take
// precedence over these weak definitions
void __attribute__ ((weak, alias ("Default_Handler")))
WDT_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
RTC_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
EFC_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
GPIOAB_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
GPIOCD_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
EXTI_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
SRAM_PARITY_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
CLKRDY_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
UART4_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
DMA_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
UART3_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
RSV_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
ADC_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
TIM1_BRK_UP_TRG_COM_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
TIM1_CC_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
TIM2_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
TIM3_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
TIM6_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
TIM7_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
TIM14_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
TIM15_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
TIM16_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
TIM17_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
I2C1_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
I2C2_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
SPI1_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
SPI2_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
UART1_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
UART2_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
LPUART_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
SPI3_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
AES_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
USB_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
DAC_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
I2S_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
GPIOEF_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
CAN1_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
CAN2_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
FPU_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
TIM4_IRQHandler(void);
void __attribute__ ((weak, alias ("Default_Handler")))
SPI4_IRQHandler(void);

// ----------------------------------------------------------------------------

extern unsigned int _estack;

typedef void (* const pHandler)(void);

// ----------------------------------------------------------------------------

// The vector table.
// This relies on the linker script to place at correct location in memory.

__attribute__ ((section(".isr_vector"),used))
pHandler __isr_vectors[] =
  { //
    (pHandler) &_estack,                          // The initial stack pointer
        Reset_Handler,                            // The reset handler

        NMI_Handler,                              // The NMI handler
        HardFault_Handler,                        // The hard fault handler

#if defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__) || defined(__ARM_ARCH_8M_MAIN__)
        MemManage_Handler,                        // The MPU fault handler
        BusFault_Handler,                         // The bus fault handler
        UsageFault_Handler,                       // The usage fault handler
#else
        0, 0, 0,				  // Reserved
#endif
        0,                                        // Reserved
        0,                                        // Reserved
        0,                                        // Reserved
        0,                                        // Reserved
        SVC_Handler,                              // SVCall handler
#if defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7EM__) || defined(__ARM_ARCH_8M_MAIN__)
        DebugMon_Handler,                         // Debug monitor handler
#else
        0,					  // Reserved
#endif
        0,                                        // Reserved
        PendSV_Handler,                           // The PendSV handler
        SysTick_Handler,                          // The SysTick handler

        // ----------------------------------------------------------------------
        // acm32f4 vectors
        WDT_IRQHandler,
        RTC_IRQHandler,
        EFC_IRQHandler,
        GPIOAB_IRQHandler,
        GPIOCD_IRQHandler,
        EXTI_IRQHandler,
        SRAM_PARITY_IRQHandler,
        CLKRDY_IRQHandler,
        UART4_IRQHandler,
        DMA_IRQHandler,
        UART3_IRQHandler,
        RSV_IRQHandler,
        ADC_IRQHandler,
        TIM1_BRK_UP_TRG_COM_IRQHandler,
        TIM1_CC_IRQHandler,
        TIM2_IRQHandler,
        TIM3_IRQHandler,
        TIM6_IRQHandler,
        TIM7_IRQHandler,
        TIM14_IRQHandler,
        TIM15_IRQHandler,
        TIM16_IRQHandler,
        TIM17_IRQHandler,
        I2C1_IRQHandler,
        I2C2_IRQHandler,
        SPI1_IRQHandler,
        SPI2_IRQHandler,
        UART1_IRQHandler,
        UART2_IRQHandler,
        LPUART_IRQHandler,
        SPI3_IRQHandler,
        AES_IRQHandler,
        USB_IRQHandler,
        DAC_IRQHandler,
        I2S_IRQHandler,
        GPIOEF_IRQHandler,
        CAN1_IRQHandler,
        CAN2_IRQHandler,
        FPU_IRQHandler,
        TIM4_IRQHandler,
        SPI4_IRQHandler,
    };

// ----------------------------------------------------------------------------

// Processor ends up here if an unexpected interrupt occurs or a specific
// handler is not present in the application code.

// ----------------------------------------------------------------------------
